ORG 100H

; UNTERPROGRAMME UND STACK

; Beschreiben Sie den Verlauf des Stackbereiches !!
; Auf welchen Adressen werden welche Werte zu
;   welchem Zweck abgelegt ?
; Beobachten Sie den Stackpointer und den Stack!
; (Fenster Register und Memory)

schalt:	equ 0	; Adresse Schalter

start:	mov ax,1234h		; Laden von AX mit dem Wert 0x1234
	mov bx,8000h		; Laden von BX - || - 0x8000
	call spwr		; Aufurfen des UP spwr
	mov si,bx		; SI mit dem Wert von BX laden
	mov di,ax		; DI mit dem Wert von AX laden
	jmp start		; Springe an Stelle start

buf:	db 0, 0

spwr:	push ax			; Speichern des Wertes AX auf dem Stack
	push bx			; Speichern des Wertes BX auf dem Stack 
	mov bx,buf		; BX mit dem Wert 0 laden
	in al,schalt		; Einlesen der Schalter
	mov [bx],al		; Schreiben in Speicher an Stelle BX mit Wert von AL
	call zeit		; Aufrufen des UP Zeit
	in al,schalt		; Einlesen des Schalters nach AL
	mov [bx+1],al		; Schreiben in Speicher an Stelle BX + 1 mit Wert von AL
	call zeit		; Aufrufen des UP Zeit
	pop bx			; Laden des obersten Stackwertes nach BX
	pop ax			; Laden des obersten Stackwertes nach AX
	ret			; Rueckkehr zum Aufruferstelle
	
	
zeit:	push cx			; Popen des Speichers 
	xor cx,cx		; XOR Verknuepfung mit CX 
schl:	loop schl		; Schleife solange CX ungleich 0 ist (wird dekrementiert)
	pop cx			; CX wird von Stack geladen
	ret			; Rueckkehr zum Aufruferstelle
