	ORG 100H

main:	MOV AL, 0x9
	CALL scode


scode:	PUSH BX

	MOV AH, 0
	MOV SI, AX
	MOV BX, werte
	MOV AL, [BX + SI]

	POP BX
	RET




;;; Datenstruktur fuer die Bits der Anzeige
werte:	DB 0b00111111		; 0
	DB 0b00000110		; 1
	DB 0b01011011		; 2
	DB 0b01001111		; 3
	DB 0b01100110		; 4
	DB 0b01101101		; 5
	DB 0b01111101		; 6
	DB 0b00000111		; 7
	DB 0b11111111		; 8
	DB 0b01101111		; 9
	DB 0b01110111		; A
	DB 0b01111100		; b
	DB 0b00111001		; C
	DB 0b01011110		; d
	DB 0b01111001		; E
	DB 0b01110001		; F
