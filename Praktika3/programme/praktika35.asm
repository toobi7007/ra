	ORG 100H

main:	MOV AL, 0		; Anzeige reseten
	OUT 0, AL
	IN AL, 0		; Schalter einlesen
	MOV AH, AL 		; Backup
t1:	OR AL, 0
	JNZ t2
	JMP ende

t2:	MOV AL, AH
	CMP AL, 1
	JNZ t3
	;; Blinke rechts
t2s:	MOV AL, 0b00001111
	OUT 0, AL
	CALL zeit
	MOV AL, 0
	OUT 0, AL
	JMP t2s

t3:	MOV AL, AH
	CMP AL, 0b10000000
	JNZ t4
	;; Blinke links
t3s:	MOV AL, 0b11110000
	OUT 0, AL
	CALL zeit
	MOV AL, 0
	OUT 0, AL
	JMP t3s

t4:	MOV AL, AH
	CMP AL, 0b10000001
	JNZ ende
	;; Warnblinke
t4s:	MOV AL, 0b11100111
	OUT 0, AL
	CALL zeit
	MOV AL, 0
	OUT 0, AL
	JMP t4s

ende:	MOV AL, 0xFF
	OUT 0, AL
	
zeit:	MOV CX, 0		; Zeitfunktion
ZTW:	LOOP ZTW
	RET
