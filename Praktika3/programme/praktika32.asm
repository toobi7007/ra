	ORG 100H

main:	MOV AL, 0
	OUT 0, AL		; Anzeige loeschen

rotL:	MOV AL, 0b00000001	; Initialisieren
rotLW:	OUT 0, AL
	ROL AL, 1		; Immer eins nach links rotieren
	CALL zeit
	CMP AL, 0b10000000	; Pruefen ob AL = 0b10000000
	JZ rotR
	JMP rotLW


rotR:	MOV AL, 0b10000000
rotRW:	OUT 0, AL
	ROR AL, 1
	CALL zeit
	CMP AL, 0b00000001
	JZ rotL
	JMP rotRW

zeit:	MOV CX, 0
ZTW:	LOOP ZTW
	RET
