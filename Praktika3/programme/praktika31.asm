	ORG 100H

main:	MOV AL, 0
	OUT 0, AL		; Loeschen der Anzeige

	IN AL, 0		; Schalter einlesen
	MOV AH, 0b00000001	; Muster setzen, der erste Schalter

	CMP AL, AH		; Pruefen ob das erste Bit sitzt
	JNZ true		; Springt wenn es 0 ist
	MOV AL, 0xFF		; Alle LEDs anmachen
	OUT 0, AL		; LEDs ausgeben

true:	MOV AL, 0b10101010
	OUT 0, AL
