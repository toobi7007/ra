	ORG 100H

main:	MOV AL, 0
	OUT 0, AL

	IN AL, 0
	MOV BL, 0b00000001 	; Muster fuer Start des Blinkens
	MOV BH, 0b00100000	; Frequenz 1 = s und 0 = l

	SHR AL, 1
	JNC nthg
	MOV CL, 6
	SHR AL, CL
	JNC lgs

snl:	MOV AL, 0xFF
	OUT 0, AL
	MOV AL, 0
	OUT 0, AL
	JMP snl

lgs:	MOV AL, 0xff
	OUT 0, AL
	MOV AL, 0
	OUT 0, AL
	CALL zeit
	JMP lgs


nthg:	MOV AL, 0b01011010
	OUT 0, AL


zeit:	MOV CX, 0		; Zeitfunktion
ZTW:	LOOP ZTW
	RET

