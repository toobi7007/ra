	ORG 100H		; Ladet das Programm an der Stelle 100H im Sp. 

start:	mov al,01010101b 	; Initialisiert das Regist AL mit 01010101b
	mov ah,0aah		; Initialisiert das Regist AH mit 0xAAH 
	mov cx,ax		; Initialisiert das Regist CX mit AX

	mov [0150h],al		; Speichert den Wert von AL im Speicher 0150H
	mov [0152h],cx		; Speichert CX im Speicher an der Adresse 150H

	add ch,cl 		; Addiert CH mit CL, CH := CH + CL

	mov bx,2 		; Initialisiert BX mit 2
	dec bx			; Dekrementiert BX mit 1
	dec bx 			; Dekrementiert BX mit 1
	dec bx 			; Dekrementiert BX mit 1

	mov dx,7700h		; Weisst DX mit dem Wert 0x7700H zu
	not bh			; Das Register BH wird Bitweise invertiert
	or  dl,bl 		; DL und BL werden Bitweise zu DL verknuepft (OR)
	and dh,11001100b	; UND Verknuepfung mit 1101100 und DH zu DH

	mov al,00000110b 	; AL wird dem Wert 110B  zugewiesen
	mov cx,3 		; CX wird dem Wert 3 zugewiesen
schl:	rol al,1		; AL wird nach Links um 1 rotiert
	out 0,al 		; AL wird an Port 0 (LED) ausgegeben
	loop schl
	;; Springt zu schl, solange CX != 0 ist und dekrementiert  
	       
	mov bl,00000110b	; Weisst BL mit dem Wert 110B zu
	mov cl,3		; Weisst CL den Wert 3 zu
	shr bl,cl		; Scheibt den Inhalt von BL um CL nach Rechts

	mov al,-1		; Weisst AL den Wert 0xff zu 
	out 90h,al 		; Weisst Port 90H den Wert von AL zu
	not al 			; AL wird Bitweise invertiert
	out 92h,al		; Port 92H = AL
	out 94h,al		; Port 94H = AL
	       
	jmp start		; Springt an Marke start
