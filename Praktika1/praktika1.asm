	ORG 100H

main:	IN AL, 0		; Einlesen auf dem Port 0 (Schalter)
	OUT 0, AL		; Ausgaben von AL auf Port (LED)

	MOV CX, 0xff		; Zaehler fuer Zeitschleife laden
schl1:	LOOP schl1
	
	MOV AL, 0 		; Register AL mit Null initialisieren
	OUT 0, AL		; Port 0 mit dem Inhalt von AL initialisierung

	MOV CX, 255
schl2:	LOOP schl2

	JMP main
