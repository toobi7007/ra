	ORG 100H

LED:	equ 00H

start:	MOV BX, 1234H 		; Lade BX mit 1234H fuer Sichern von Kontext
	;; LED an
	MOV al, 0FFH
	OUT LED, al

	;; Pause
	CALl pause
	
	;; LED aus
	MOV al, 0
	OUT LED, al


	;; Pause
	CALL pause
	
	JMP start


pause: 	PUSH BX			; Sicherheitskopie BX

	MOV BX, 40000
P1:	DEC bx
	JNZ P1

	POP BX 			; Sicherheitskopie von BX von Laden

	RET
