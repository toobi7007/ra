null:	equ 0x3F		; Symbolischen Link erstellen -> null = 0x3F
ste1:	equ 0x92		; symbolischen Link erstellen -> ste1 = 0x92

	;; Was wird jeweils geladen?
	MOV CX, [var16]		; CX wird mit dem Wert aus dem Speicher der Zelle var16 geladen
	MOV BX, var8		; BX wird mit dem Wert von var8 geladen
	MOV AL, [BX]		; AL wird mit dem Wert aus dem Speicher der Zelle BX geladen
	MOV [var8], AL		; Der Speicher der Zelle var8 wird mit AL geladen
	MOV DX, ste1		; DX wird mit dem Wert 0x92 geladen
	MOV AL, null		; AL wird 0x3F geladen
	OUT DX, AL		; An Port des Wertes von DX wird AL geladen
	OR byte [0120H], 0xeeee
	;; Gibt bei relativen Adressen den Datentyp an, hier 1 Byte bzw. wie viel Speicher
	MOV word [0x200], 1
	;; Hier 2 Byte

var8:	db 0			; Reservieren von Speicher, 1 Byte wird reserviert
var16:	dw 1			; Reservieren von Speicher, 2 Byte werden reserviert
	
