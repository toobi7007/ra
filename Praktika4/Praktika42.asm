	ORG 100H

main:	MOV AL, 0
	MOV WORD DX, 0x90
schl1:	OUT DX, AL
	INC DX
	INC DX
	CMP DX, 0x98
	JNZ schl1
	OUT DX, AL

	;; Werte von 0 bis 3 einlesen
	IN AL, 0

	;; Alle anderen Schalter bis auf die ersten 3 Bits loeschen
	MOV CL, 4
	SHL AL, CL
	SHR AL, CL
	
	;; Ausgabe der Zahl
	MOV SI, AX
	MOV BX, werte
	MOV AL, [BX + SI]
	OUT 0x90, AL

werte:	DB 0b00111111		; 0
	DB 0b00000110		; 1
	DB 0b01011011		; 2
	DB 0b01001111		; 3
	DB 0b01100110		; 4
	DB 0b01101101		; 5
	DB 0b01111101		; 6
	DB 0b00000111		; 7
	DB 0b11111111		; 8
	DB 0b01101111		; 9
	DB 0b01110111		; A
	DB 0b01111100		; b
	DB 0b00111001		; C
	DB 0b01011110		; d
	DB 0b01111001		; E
	DB 0b01110001		; F
