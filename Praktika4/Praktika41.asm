	ORG 100H
	JMP AufC

	;; Aufgabe a) (Nur bis 0x20F, fuer 0x2FF auf 0x300 erhoehen)
AufA:	IN AL, 0		; Einlesen der Schalter

	MOV SI, 0x200		; Pointer auf 0x200 setzen
schl1:	MOV [SI], AL		; Im Speicher an der Stelle SI wird mit Inhalt AL geladen
	INC SI			; SI wird eine Speicherzelle weiter gesetzt
	CMP SI, 0x210		; Vergleiche SI mit Endwert
	JNZ schl1		; Falls er ist, springe nicht mehr zu Schleife 1

	;; Aufgabe b)
	;; Nur von 0x210 bis 0x21F, fuer 0x200 bis 0x2FF einfach Werte aendern
AufB:	MOV SI, 0x210		; Startwert fuer Adresse
	MOV AL, 0x10		; Anfangswert fuer Inhalt

schl2:	MOV [SI], AL
	INC SI
	INC AL
	CMP AL, 0x20
	JNZ schl2
	MOV [SI], AL
	
	;; Aufgabe c)
	;; Wieder wie bei den letzten
AufC:	MOV SI, 0x200		; Kopiere von ...
	MOV DI, 0x300		; ... zu ...

schl3:	MOV AL, [SI]
	MOV [DI], AL
	INC DI
	INC SI

	CMP SI, 0x210
	JNZ schl3
