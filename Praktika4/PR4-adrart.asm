
        ORG 100H
;----------------------------------------

; PRAKTIKUM 4:
; Adressierungsarten

; Kommentieren (Beschreiben) Sie folgende Befehle bzw.
; Programmablaeufe!
; Welche konkreten Zahlenwerte werden gelesen bzw.
; geschrieben?
; Was ist der Unterschied zwischen EQU und DB?
	;; EQU symbolischer Link
	;; DB Speichert im speicher
; Wie zeigen sich die so definierten Werte im fertigen
; Maschinencode ?


scha:	equ 0		;Adresse Schalter
leds:	equ 0		;Adresse LEDs
anz0:	equ 90h		;Adresse rechte 7-Segm-Anzeige
anz1:   equ 92h

start:
	mov al,[vari8a]		; Laedt AL mit 0x55 aus der Adresse vari8a (0x140 z.b.)
	not al			; AL wird bitweise Invertiert
	mov [vari8a],al		; AL wird in Speicher an der Stelle vari8a geschrieben

	mov byte [vari8b], 88h	; Im Speicher an der Stelle vari8b wird 1 Byte mit 0x88 beschrieben
	inc word [vari16]	; Der 16 Bit Block an Position vari16 wird um eins erhoeht 
	
	mov bx,textanf		; BX wird mit den Werten von textanf beschrieben
	mov cl,[bx]		; CL wird mit dem 8 Bit Inhalt von Adresse BX geladen
	mov ch,[bx+3]		; CH wird mit dem 8 Bit Inhalt von Adresse BX + 3 geladen
	mov dl,[textend]	; DL wird mit dem 8 Bit Inhalt von Adresse textend geladen (0xFF)
	;; |--> 0xFF ist inhalt von [textend] und textend adresse ist bspw. 0x14A

        mov al,01000000b	; Laedt AL mit 0b0100 0000
        mov dx,anz0		; Laedt DX mit 0x90
        out dx,al        ; im Display anzeigen
        inc dx		 ; DX wird um 1 erhoeht
        inc dx		 ; DX wird um 1 erhoeht
	out dx,al        ; naechste Stelle
	inc dx		 ; DX wird um 1 erhoeht
        inc dx		 ; DX wird um 1 erhoeht
	out dx,al        ; ...

	mov cx,4		; CX wird mit 4 geladen
	mov si,0		; Adressregister SI wird mit 0 geladen
schl:	mov bx,codetab		; BX wird mit Adresse von codetab geladen 
	mov al,[bx+si]		; AL wird der relativen Adresse BX + SI geladen (SI = Pointer)
	out anz1,al		; AL wird auf Port anz1 ausgegeben bzw. geladen
	inc si			; SI wird im 1 erhoeht
	loop schl		; Schleife
		
	nop			
	
	jmp start
	
	dd 0
 
vari8a:	db 55h
vari8b:	db 0
vari16:	dw 1234h

textanf: db "ABCabc"
textend: db 0ffh

codetab: 
		db 00000110b	;1
		db 01011011b	;2
		db 01001111b	;3
		db 01100110b	;4

;----------------------------------------

